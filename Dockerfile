FROM debian:bookworm

ARG APP_UID=2000

ARG APP_GID=2000

LABEL maintainer="TANGO Controls team <contact@tango-controls.org>"

ENV DEBIAN_FRONTEND=noninteractive
ENV CMAKE_PREFIX_PATH=/home/tango/lib/cmake
ENV PKG_CONFIG_PATH=/home/tango/lib/pkgconfig

# Evaluated by install_omniorb.sh and install_catch.sh
ENV CROSSCOMPILE=i686

RUN dpkg --add-architecture i386

# apt-get install needs two invocations
# to avoid an unreachable package error
RUN apt-get update &&                        \
  apt-get install -y --no-install-recommends \
  gcc-i686-linux-gnu &&                      \
  apt-get install -y --no-install-recommends \
  apt-utils                                  \
  build-essential                            \
  bzip2                                      \
  ca-certificates                            \
  cmake                                      \
  cppzmq-dev                                 \
  curl                                       \
  g++-multilib                               \
  gcc-multilib                               \
  git                                        \
  libssl-dev:i386                            \
  libzmq3-dev:i386                           \
  libzstd-dev:i386                           \
  pkg-config:i386                            \
  pkg-config                                 \
  python3:i386                               \
  python3-dev:i386                           \
  zlib1g-dev &&                              \
  rm -rf /var/lib/apt/lists/*

RUN groupadd -g "$APP_GID" tango                            && \
    useradd -l -u "$APP_UID" -g "$APP_GID" -ms /bin/bash tango

USER tango

WORKDIR /home/tango

COPY --chown="$APP_UID":"$APP_GID" scripts/*.sh ./
COPY --chown="$APP_UID":"$APP_GID" scripts/patches/* ./patches/

RUN  ./common_setup.sh      && \
     ./install_tango_idl.sh && \
     ./install_omniorb.sh   && \
    ./install_catch.sh      && \
     rm -rf dependencies
